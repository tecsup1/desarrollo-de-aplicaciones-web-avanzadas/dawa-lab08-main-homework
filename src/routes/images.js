const express = require('express');

const app = express();
app.set('views', './src/views'); // indicando donde está la carpeta view

const Usuario = require("../models/usuario");

const fs = require("fs");
const path = require("path");

app.get("/imagenes",(req,res)=>{
    Usuario.find({}).exec((err, usuarios) => {
        res.render("imagesView", {
            usuarios: usuarios,
        });
    });
    // res.render('imagesView')
})


app.get("/imagen/:tipo/:img", (req,res)=>{
    let tipo = req.params.tipo;
    let img = req.params.img;

    // TODO: corregir para heroku
    let pathImagen = path.resolve(__dirname, `../../uploads/${tipo}/${img}`);


    if(fs.existsSync(pathImagen)){
        res.sendFile(pathImagen);
    }else{
        let noImagePath = path.resolve(__dirname, `../assets/no-image.jpg`);
        res.sendFile(noImagePath);
    }
});

module.exports = app;