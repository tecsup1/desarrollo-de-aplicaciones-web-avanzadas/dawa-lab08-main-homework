
const express = require("express");

const app = express();
app.set('views', './src/views'); // indicando donde está la carpeta view

app.use(require("./usuario"));
app.use(require("./login"));

// lab11:
app.use(require("./upload"));

app.use(require("./images"));

module.exports = app