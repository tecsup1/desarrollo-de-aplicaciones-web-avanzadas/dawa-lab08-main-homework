const express = require("express");
const fileUpload = require("express-fileupload");
const app = express();

const Usuario = require("../models/usuario");

const fs = require('fs');
const path = require('path');

const { verificaToken } = require("../middlewares/authentication");

app.set('views', './src/views'); // indicando donde está la carpeta view


// default options
app.use(fileUpload({ useTempFiles: true }));

// app.post("/upload/:tipo/:id", function (req, res) {
app.post("/upload",verificaToken, function (req, res) {
    // let tipo = req.params.tipo;
    // let id = req.params.id;
    let tipo = req.body.tipo;
    let id = req.body.id;

    // analiza si existe el archivo subido
    if (!req.files) {
        res.render("error",{
            err:'No se ha seleccionado ningún archivo.',
        });
    }

    // validar tipo
    let tiposValidos = ["productos", "usuarios"];
    if (tiposValidos.indexOf(tipo) < 0) {
        // return res.status(400).json({
        //     ok: false,
        //     err: {
        //         message: "Los tipos permitidos son " + tiposValidos.join(", "),
        //     },
        // });
        return res.render("error",{
            err:"Los tipos permitidos son " + tiposValidos.join(", "),
        });

    }

    let archivo = req.files.archivo;
    let nombreArchivoCortado = archivo.name.split(".");
    let extension = nombreArchivoCortado[nombreArchivoCortado.length - 1];
    console.log("extension: 0",extension);
    // Extensions permitidas
    let extensionesValidas = ["png", "jpg", "gif", "jpeg"];
    if (extensionesValidas.indexOf(extension) < 0) {
        console.log("entramos en el error");

        return res.render("error",{
            err:"Las extensiones permitidas son "+extensionesValidas.join(", "),
        });
    }

    // cambiar nombre al archivo:
    let nombreArchivo = `${id}-${new Date().getMilliseconds()}.${extension}`;
    let rutaArchivo2 = `${tipo}/${nombreArchivo}`;

    // archivo.mv("uploads/filename.jpg", (err)=>{

    // TODO: corregir para heroku
    // archivo.mv(`uploads/${tipo}/${nombreArchivo}`, (err) => {
    archivo.mv(`uploads/${tipo}/${nombreArchivo}`, (err) => {

        if (err) {
            return res.status(500).json({
                ok: false,
                err,
                chalius: "aquí estamos"
            });
        }
        // res.json({
        //     ok: true,
        //     message: "Imagen subida correctamente",
        // });
        imagenUsuario(id, res, rutaArchivo2);
    });
});

// añade el nombre del archivo al objeto usuario en la BD
function imagenUsuario(id, res, nombreArchivo){
    Usuario.findById(id, (err, usuarioDB)=>{
        if(err){
            // return res.status(500).json({
            //     ok: false,
            //     err,
            // });
            return res.render("error",{
                err,
            });
        }

        if(!usuarioDB){
            // return res.status(400).json({
            //     ok: false,
                // err:{
                //     message:"Usuario no existe",
                // },
            // });

            return res.render("error",{
                err:{
                    message:"Usuario no existe",
                },
            });
        }

        // para borrar archivo del servidor(punto 4.4 lab11):
        // let pathImagen = path.resolve(__dirname, `../../uploads/usuarios/${usuarioDB.img}`);
        let pathImagen = path.resolve(__dirname, `../../uploads/${usuarioDB.ruta_img}`);

        if(fs.existsSync(pathImagen)){
            fs.unlinkSync(pathImagen)
        }

        usuarioDB.ruta_img = nombreArchivo;

        usuarioDB.save((err, usuarioGuardado)=>{
            // res.json({
            //     ok: true,
            //     usuario: usuarioGuardado,
            //     ruta_img: nombreArchivo,
            // });
            res.redirect('/usuario_listar')
         


        });
    });
}


module.exports = app;
